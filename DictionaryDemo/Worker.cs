﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DictionaryDemo
{
    public class Worker
    {
        public Worker()
        {
            Work();
        }

        public void Work()
        {
            Dictionary<string, Person> dict = new Dictionary<string, Person>();
            Person george = new Person() { Name = "George Washington", Age = 67 };
            string key = "George";
            dict.Add(key, george);

            dict.Add("john", new DictionaryDemo.Person() { Name = "John Adams", Age = 94 }); // shortcut for adding in the dictionary
            dict.Add("thom", new DictionaryDemo.Person() { Name = "Thomas Jefferson", Age = 89 });
            dict.Add("james", new DictionaryDemo.Person() { Name = "James Madison", Age = 87 });

            Person secondPresident = dict["john"];
            Console.WriteLine($"The second president was: {secondPresident.Name}");
        }
    }
}
