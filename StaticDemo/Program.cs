﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StaticDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            string selection = string.Empty;
            while (selection != "q" && selection != "Q")
            {
                Console.Write("Enter C)elsius to Fahrenheit or F)arenheit to Celsius or Q)uit:");
                selection = Console.ReadLine();
                double farenheit = 0, celsius = 0;
                switch (selection)
                {
                    case "C":
                    case "c":
                        Console.Write("Please enter the Celsuis temperature: ");
                        farenheit = TemperatureConverter.CelsuisToFahrenheit(Console.ReadLine());
                        Console.WriteLine($"Temperature in Fahrenheit: {farenheit:f2}");
                        break;

                    case "F":
                    case "f":
                        Console.Write("Please enter the Fahrenheit temperature: ");
                        celsius = TemperatureConverter.FahrenheitToCelsius(Console.ReadLine());
                        Console.WriteLine($"Temperature in Fahrenheit: {celsius:f2}");
                        break;

                    case "Q":
                    case "q":
                        break;

                    default:
                        Console.WriteLine("Please try again");
                        break;

                }
            }
        }
    }
}
