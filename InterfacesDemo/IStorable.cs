﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfacesDemo
{
    public interface IStorable // Interface Class
    {
        void Read(String fileName);
        void Write(String fileName);
        // IStorable have Read and Write only
    }
}
