﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfacesDemo
{
    public interface ISpeak // Interface Class
    {
        double DurationOfSpeech { get;  }
        void Speak();
    }
}
