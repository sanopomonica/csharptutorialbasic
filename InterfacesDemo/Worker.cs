﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfacesDemo
{
    public class Worker
    {
        public Worker()
        {
            Work();
        }
        public void Work()
        {
            Document doc1 = new Document();
            doc1.Reformat();
            doc1.Read("myFile");
            doc1.Speak();

            IStorable doc2 = new Document();
            doc2.Read("myFile");
            doc2.Write("myFile");
            //doc2.Speak(); //will have an error coz Speak is not part of IStorable

            ISpeak doc3 = new Document();
            doc3.Speak();
            //doc3.Read(); // error, not part of ISpeak

            Document doc4 = new Memo();
            doc4.Reformat();
            doc4.Read("MyFile");
            //doc4.MemoTo = "Jesse"; // error, not part of Document

            Memo doc5 = new Memo();
            doc5.Read("MyFile");
            doc5.Reformat();
            doc5.MemoTo = "Jesse";
        }

    }
}
