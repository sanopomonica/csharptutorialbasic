﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LambdaExpressions
{
	class Program
	{
		public static void Main(string[] args)
		{
			int a = 10;
			int b = 15;
			int c = 5;
			Func<int, int, int, int> multiplyDelegate = (x, y, z) => (x * y * z);
			int product = multiplyDelegate(a, b, c);
			Console.WriteLine(product);

			Func<int, int, int, int> multiplyDelegate1 = (x, y, z) =>
			{

				return x * y * z;
			};

		}
	}
}
